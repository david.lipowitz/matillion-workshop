# Accelerate Data Automation with Matillion and Amazon Redshift
### Overview
1.  Creating a Matillion Stack via Cloud Formation
1.  Matillion Exercises

### Creating a Matillion Stack via Cloud Formation
##### Prerequisites:
1.  Subscribe to [Matillion ETL for Amazon Redshift](https://aws.amazon.com/marketplace/pp/B010ED5YF8?qid=1532546478798&sr=0-1&ref_=srh_res_product_title) on AWS Marketplace
1.  [Create a Key Pair](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair)
    * Save the downloaded .pem file in a safe place
1.  [Create an S3 Bucket](https://docs.aws.amazon.com/AmazonS3/latest/gsg/CreatingABucket.html)
1.  Download this template: [poc_new.json](https://gitlab.com/david.lipowitz/matillion-workshop/blob/master/poc_new.json)

##### Instructions: [Launching Matillion ETL Using Templates](https://redshift-support.matillion.com/s/article/2858081)
1.  Create a new stack through the CloudFormation Console
    *   Specify template
        *  Upload poc_new.json template file
    *  Specify stack details -- must set items below, otherwise user defaults
        *  Stack name -- The name of your CloudFormation stack
        *  Keypair name -- From prerequisites above, the selected key pair will be added to the set of keys authorized for this instance
        *  AZ #1 -- Select an Availability Zone from the current region
        *  Inbound IPv4 CIDR -- Inbound IPv4 CIDR Range for Matillion Instance e.g. `0.0.0.0/0`
        *  Master password -- Redshift Password. Must contain one upper and one lower case letter and one digit but not quotes, slashes, @ or spaces.
    *  Configure stack options
        *   None required
    * Create stack
        * Check box to acknowledge that AWS CloudFormation might create IAM resources.
1.  5 to 10 minutes to spin up
1.  Optionally allow Redshift queries from your laptop
    *  Open Redshift cluster to public
        *  Modify Cluster
        *  Set "Publicly accessible" to `Yes`
    *  Allow traffic through cluster's Security Group


### Matillion Exercises
1.  [Create a project](https://redshift-support.matillion.com/s/article/1991963)
    *  Creates a space to create your jobs
    *  Stores AWS and Redshift credentials for use by Matillion
1.  [S3 Load Generator](https://redshift-support.matillion.com/s/article/2973428)
    * Publicly available flight data, use `s3://mtln-workshop/flights/2008.csv.gz`
    * Change the table name from `2008.csv` to `flights`
    * Create fault tolerant staging tables: [stg_flights.txt](https://gitlab.com/david.lipowitz/matillion-workshop/blob/master/stg_flights.txt) 
1.  [Iteration](https://redshift-support.matillion.com/s/article/2980027)
    *  Parameterize S3 Load component with `${year}` variable
    *  Note that loading files with a matching prefix will perform better than a loop
1.  [Handling Data Quality Issues through Transformation](https://redshift-support.matillion.com/s/article/2822021)
    *  Funnel off records with NULL `TailNum` for inspection
    *  Redirect records with valid `TailNum`s to table for use in later analytics
1.  [JDBC Incremental Load](https://redshift-support.matillion.com/s/article/2955330)
    * Connection Details 
        * Connection URL: `jdbc:postgresql://pgdemo.cq5i4y35n9gg.eu-west-1.rds.amazonaws.com/mtln`
        * Username: `workshop`
        * Password: Use Password Manager to add `workshop-pgdemo` entry for password `Workshop1234`
    * Connection Options:
        * Set `currentSchema` to `workshop`
    * Data Sources:
        * Add `orders` and `order_items`
    * Source Columns:
        * Set `update_date` as Incremental column for both `orders` and `order_items`
    * Configuration:
        * Staging Bucket: Your S3 Bucket
        * Staging Table Prefix: `stage_`
        * Staging Schema: `public`
        * Target Table Prefix: `target_`
        * Target Schema: `public`
        * Target Distribution Style: `auto`
        * Concurrency: `Sequential` <-- please!
1.  [Event-driven Workflows using SQS](https://redshift-support.matillion.com/s/article/2144265)
    *  Microbatching workflow to refresh `order` and `order_item` data
    *  Create an SQS Queue and configure Matillion to listen to it
1.  [Notification using SNS](https://redshift-support.matillion.com/s/article/2103738)
    *   Send notification in case of job failures
    *   Create an SNS Topic and subscribe your email to it
    *   Configure SNS Component to send a message to that topic


### Sample Jobs: [aws_dev_day_samples.json](https://gitlab.com/david.lipowitz/matillion-workshop/blob/master/aws_dev_day_samples.json)
